# XUI Designs

This is a collection of various design from various sources such as [dribbble](https://dribbble.com) implemented in vue 3.

You can view the site [here](https://xui-designs.gitlab.io/vue3/)

## Running
To run this project locally [VSCode](https://code.visualstudio.com/) is the recommended IDE to use and as such is the assumed IDE henceforth.

### Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

### Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Project Setup
Install dependencies

```sh
yarn install
```

Compile and Hot-Reload for Development

```sh
yarn dev
```

Type-Check, Compile and Minify for Production

```sh
yarn build
```

Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
yarn test:unit
```

Run End-to-End Tests with [Cypress](https://www.cypress.io/)

```sh
yarn build
yarn test:e2e # or `yarn test:e2e:ci` for headless testing
```

Lint with [ESLint](https://eslint.org/)

```sh
yarn lint
```
