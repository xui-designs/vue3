import { fileURLToPath, URL } from 'url';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const isProd = mode === 'production';

  return {
    plugins: [vue()],
    base: isProd ? '/vue3/' : '/',
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
        '@designs': fileURLToPath(new URL('./src/features/designs', import.meta.url)),
        '@widgets': fileURLToPath(new URL('./src/features/widgets', import.meta.url)),
      },
    },
  };
});
