import { Config } from 'tailwindcss';
import colors from 'tailwindcss/colors';

const config = {
  darkMode: 'class',
  content: [
    './index.html',
    './public/*.html',
    './src/**/*.{vue,js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: 'hsl(var(--color-primary) / <alpha-value>)',
          dark: 'hsl(var(--color-primary-dark) / <alpha-value>)',
        },
        'on-primary': {
          DEFAULT: '#fff',
          dark: '#fff',
        },
        
				// text-colors
        light: colors.blue[950],
        dark: colors.slate[200],

        // design specific colors
        d: {
          payment: {
            DEFAULT: '#0066ff',
            text: '#1a1d21',
            highlight: '#272a30',
          },
        },
      },
    },
  },
  plugins: [require('@tailwindcss/forms')],
} satisfies Config;

export default config;
