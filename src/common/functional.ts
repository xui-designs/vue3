export function isXS(): boolean {
  return window.innerWidth < 640;
}

export function isSM(): boolean {
  return window.innerWidth >= 640;
}

export function isMD(): boolean {
  return window.innerWidth >= 1024;
}

export function setTheme(theme: 'light' | 'dark'): void {
  localStorage.theme = theme;

  const el = document.documentElement;
  if (theme === 'light') {
    el.classList.remove('dark');
    el.classList.add('light');
  } else {
    el.classList.remove('light');
    el.classList.add('dark');
  }
}

export function useOSTheme(): void {
  localStorage.removeItem('theme');
}

export function isDarkMode(): boolean {
  return document.documentElement.classList.contains('dark');
}

export function getLocalAssetUrl(url: string, baseUrl: string): string {
  return new URL(url, baseUrl).href;
}
