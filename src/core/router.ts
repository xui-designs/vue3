import {
  createRouter,
  createWebHashHistory,
  createWebHistory,
  type RouteRecordRaw,
} from 'vue-router';

import { homeRoutes } from '@/features/home/routes';
import { designRoutes } from '@designs/routes';
import { widgetsRoutes } from '@widgets/routes'

const routes: RouteRecordRaw[] = [
	...homeRoutes,
	...designRoutes,
	...widgetsRoutes,
];

const router = createRouter({
  history: import.meta.env.PROD
    ? createWebHashHistory()
    : createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
