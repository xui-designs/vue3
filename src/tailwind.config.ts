import { Config } from 'tailwindcss'
import colors from 'tailwindcss/colors'

const config = {
  content: ['./index.html', './src/**/*.{js,jsx,ts,tsx,vue}'],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        primary: {
					DEFAULT: colors.orange[500],
					dark: colors.orange[700],
        },
        'on-primary': {
          DEFAULT: '#fff',
          dark: '#fff',
        },
        secondary: {
					DEFAULT: colors.green[500],
					dark: colors.green[700],
        },
				container: {
					DEFAULT: colors.white,
					dark: "hsl(25,95%,13%)",
				},

        // text-colors
        light: colors.slate[900],
        dark: colors.slate[50],

				// surface
				surface: {
					// DEFAULT: "#fafcff",
					DEFAULT: "#fff",
					dark: "#1d1d1d",
				}
      },
    },
  },
  plugins: [require('@tailwindcss/forms')],
} satisfies Config

export default config
