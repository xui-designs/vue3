import { createApp } from 'vue';
import { createPinia } from 'pinia';

import App from './App.vue';
import router from './core/router';
import meta from './plugins/meta';

import './assets/main.scss';

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(meta);

app.mount('#app');
