import type { RouteRecordRaw } from 'vue-router';

import PaymentView from './index.vue';

import OverviewRootView from './views/overview/index.vue';
import OverviewView from './views/overview/OverviewView.vue';
import NotificationsView from './views/overview/NotificationsView.vue';
import TransactionsView from './views/overview/TransactionsView.vue';

import HistoryView from './views/HistoryView.vue';
import ContactsView from './views/ContactsView.vue';
import ProductsView from './views/ProductsView.vue';

export const paymentRoutes: RouteRecordRaw[] = [
  {
    path: 'payment',
    name: 'payment',
    component: PaymentView,
    redirect: { name: 'payment-overview' },
    children: [
      {
        path: 'overview',
        component: OverviewRootView,
        children: [
          {
            path: '',
            name: 'payment-overview',
            component: OverviewView,
          },
          {
            path: 'notifications',
            name: 'payment-overview-notifications',
            component: NotificationsView,
          },
          {
            path: 'transactions',
            name: 'payment-overview-transactions',
            component: TransactionsView,
          },
        ],
      },
      { path: 'history', name: 'payment-history', component: HistoryView },
      { path: 'contacts', name: 'payment-contacts', component: ContactsView },
      { path: 'products', name: 'payment-products', component: ProductsView },
    ],
  },
];
