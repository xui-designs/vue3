import type { RouteRecordRaw } from 'vue-router';

import IndexView from './index.vue';
import DesignsView from './pages/designs.vue';

import { paymentRoutes } from './pages/payment/routes';

export const designRoutes: RouteRecordRaw[] = [
  {
    path: '/designs',
    name: 'designs',
    component: DesignsView,
  },
  {
    path: '/designs',
    component: IndexView,
    children: [...paymentRoutes],
  },
];
