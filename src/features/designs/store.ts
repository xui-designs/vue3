import { defineStore } from 'pinia';
import type { RouteLocationRaw } from 'vue-router';

export interface IDesign {
  uid: string;
  title: string;
  path: RouteLocationRaw;

  sourceUrl: string;
  sourceImageUrl: string;
  sourceVideoUrl?: string;
}

interface IState {
  designs: IDesign[];
}
const state = (): IState => ({
  designs: [
    {
      uid: 'payment-service',
      title: 'Payment Service',
      path: { name: 'payment' },
      sourceUrl: 'https://dribbble.com/shots/18217274-Fee-free-Payment-Service',
      sourceImageUrl:
        'https://cdn.dribbble.com/userupload/2712191/file/original-edabd93b69a52244dda6e824648eb0a6.png?filters:format(webp)?filters%3Aformat%28webp%29=&compress=1&resize=1200x900',
      sourceVideoUrl:
        'https://cdn.dribbble.com/userupload/2712190/file/original-706968bbefee4b450255aabf9b2e94e1.mp4',
    },
  ],
});

export const useDesginsStore = defineStore('designs-store', {
  state,
  actions: {
    getDesign(uid: string): IDesign | undefined {
      return this.designs.find((d) => d.uid === uid);
    },
  },
});
