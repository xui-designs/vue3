import { defineStore } from 'pinia';

import type { IWidget } from './interface';
import { getLocalAssetUrl } from '@/common/functional';

interface IState {
  widgets: IWidget[];
}
const state = (): IState => ({
  widgets: [
    {
      uid: 'calendar-meeting',
      title: 'Calendar Meeting',
      path: { name: 'calendar-meeting' },
      sourceImageUrl: getLocalAssetUrl('./images/calendar-meeting.png', import.meta.url),
    },
  ],
});

export const useWidgetsStore = defineStore('widgets-store', {
  state,
  actions: {
    getWidget(uid: string): IWidget | undefined {
      return this.widgets.find((w) => w.uid === uid);
    },
  },
});
