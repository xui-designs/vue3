import { RouteLocationRaw } from "vue-router";

export interface IWidget {
  uid: string;
  title: string;
  path: RouteLocationRaw;

  sourceImageUrl: string;
	sourceUrl?: string;
  sourceVideoUrl?: string;
}
