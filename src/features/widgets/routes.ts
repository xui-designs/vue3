import type { RouteRecordRaw } from 'vue-router';

import IndexView from './index.vue';
import WidgetsView from './pages/widgets.vue';
import CalendarMeetingView from './pages/calendar-meeting.vue';

export const widgetsRoutes: RouteRecordRaw[] = [
  {
    path: '/widgets',
    component: IndexView,
    children: [
      {
        path: '',
        name: 'widgets',
        component: WidgetsView,
      },
      {
        path: 'calendar-meeting',
        name: 'calendar-meeting',
        component: CalendarMeetingView,
      },
    ],
  },
];
