import { differenceInMinutes, startOfDay } from "date-fns";

interface IMeeting {
  id: number;
  title: string;
  description: string;
  by: string;
  from: Date;
  to: Date;
	image: string;
	videoLink: string;
	isConfirmed?: boolean;
}

export default class MeetingModel {
  id: number;
  title: string;
  description: string;
  by: string;
  from: Date;
  to: Date;
	image: string;
	duration: string;
	videoLink: string;
	isConfirmed?: boolean;

  constructor(data: IMeeting) {
    this.id = data.id;
    this.title = data.title;
    this.description = data.description;
    this.by = data.by;
    this.from = data.from;
    this.to = data.to;
		this.image = data.image;
		this.videoLink = data.videoLink;
		this.isConfirmed = data.isConfirmed;

		this.duration = this._setDuration()
  }

  private _setDuration(): string {
    const mins = differenceInMinutes(this.to, this.from);
    const hr = Math.floor(mins / 60);
    const m = mins - hr * 60;

    let duration = `${hr} hour${hr > 1 ? 's' : ''}`;
    if (m > 0) {
      duration += ` ${m} minutes`;
    }

    return duration;
  }
}
