import type { RouteRecordRaw } from 'vue-router';

import IndexView from './index.vue';

export const homeRoutes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'home',
    component: IndexView,
  },
];
