import { twMerge } from 'tailwind-merge';

const base =
  'rounded-md text-sm px-4 py-3 hover:bg-slate-200 transition-colors disabled:bg-transparent disabled:cursor-not-allowed';

const elevated = twMerge(base, 'bg-primary disabled:bg-black/30 text-on-primary hover:bg-primary-dark');

const text = twMerge(
  base,
  'bg-primary/10 text-primary-dark hover:bg-primary/20 disabled:bg-transparent disabled:text-black/50',
);
const textIcon = twMerge(text, 'flex items-center gap-x-3 [&_svg]:w-5');

const icon = twMerge(text, 'rounded-full aspect-square p-3 [&_svg]:w-5');

export const variants = {
  base,
  elevated,
  text,
  icon,
  'text-icon': textIcon,
  none: '',
};
