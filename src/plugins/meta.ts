interface IOGTags {
  url?: string;
  title?: string;
  description?: string;
  type?: string;
  image?: string;
}

interface IMetaTags {
  title?: string;
  url?: string;
  description?: string;
  og?: IOGTags;
  tags?: {
    [propName: string]: string;
  }[];
}

function handleMeta(config: IMetaTags) {
  document
    .querySelectorAll('[data-meta-controlled]')
    .forEach((t) => t.parentNode?.removeChild(t));

  if (config.title) {
    document.title = config.title;
    config.og = {
      title: config.title,
      ...config.og,
    };
  }

  if (config.description) {
    config.og = {
      description: config.description,
      ...config.og,
    };

    config.tags = [
      {
        name: 'description',
        content: config.description,
      },
      ...(config.tags ?? []),
    ];
  }

  config.og = {
    url: config.url ?? window.location.href,
    ...config.og,
  };

  if (config.tags) {
    for (const tagDef of config.tags) {
      const tag = document.createElement('meta');

      for (const [attrib, value] of Object.entries(tagDef)) {
        tag.setAttribute(attrib, value);
      }

      tag.setAttribute('data-meta-controlled', '');
      document.head.appendChild(tag);
    }
  }

  if (config.og) {
    config.og.type = config.og.type ?? 'website';
    for (const [property, value] of Object.entries(config.og)) {
      const tag = document.createElement('meta');

      tag.setAttribute('property', `og:${property}`);
      tag.setAttribute('content', value);

      tag.setAttribute('data-meta-controlled', '');
      document.head.appendChild(tag);
    }
  }
}

export type TMeta = (config: IMetaTags) => void;

export default {
  install: (app) => {
    const func = (config: IMetaTags) => {
      // console.log(config);
      handleMeta(config);
    };

    // app.config.globalProperties.$meta = func;
    app.provide('meta', func);
  },
};
